package android.vanrietj.crystalball;

import android.media.MediaPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Likely, it is.",
                "Count on it, do not.",
                "Difficult to see, always in motion is the future.",
                "Certain, it is.",
                "Decidedly so, it is.",
                "Without a doubt.",
                "Definitely yes.",
                "Mmmm, yes.",
                "Good, outlook is.",
                "Signs point to yes.",
                "Rely on it, you may.",
                "As I see it yes.",
                "Later, try again later.",
                "Tell you now, better not.",
                "Now, cannot predict now.",
                "Use the force, and ask again.",
                "No, my reply is.",
                "No, my sources say.",
                "Not so good, outlook is.",
                "Doubtful, very doubtful."
        };
    }

    public static Predictions get() {
      if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        int rand = new Random().nextInt(4);
        return answers[rand];
    }
}
